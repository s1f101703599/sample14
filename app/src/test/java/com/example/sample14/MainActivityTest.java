package com.example.sample14;

import org.junit.Before;
import org.junit.Test;
import org.junit.After;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;
import org.hamcrest.CoreMatchers;

public class MainActivityTest {
    @Test
    public void makeResult() {
        String x = "1.0";
        String y = "2.0";
        assertThat(activity.makeResult(x,y, R.id.radioButtonAddition), is("1.0+2.0=3.0"));
    }
    @Test
    public void makeResult2() {
        String x = "1.0";
        String y = "2.0";
        assertThat(activity.makeResult(x,y, R.id.radioButtonSubtraction), is("1.0-2.0=-1.0"));
    }
    @Test
    public void makeResult3() {
        String x = "1.0";
        String y = "2.0";
        assertThat(activity.makeResult(x,y, R.id.radioButtonMultiplication), is("1.0*2.0=2.0"));
    }
    @Test
    public void makeResult4() {
        String x = "1.0";
        String y = "2.0";
        assertThat(activity.makeResult(x,y, R.id.radioButtonDivision), is("1.0/2.0=0.5"));
    }
}